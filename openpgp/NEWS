                                                              -*- org -*-
#+TITLE: sequoia-openpgp NEWS – history of user-visible changes
#+STARTUP: content hidestars

* Changes in 1.4.0
** New functionality
   - Preferences::policy_uri
   - TSK::eq
   - ValidAmalgamation::revocation_keys
   - ValidCert::policy_uri
   - ValidCert::revocation_keys
** Notable fixes
   - Filters set using CertParser::unvalidated_cert_filter are now
     preserved during iterations.
* Changes in 1.3.0
** New functionality
   - CertBuilder::add_subkey_with
   - CertBuilder::add_user_attribute_with
   - CertBuilder::add_userid_with
   - ComponentBundle::attestations
   - Encryptor::with_session_key
   - Signature::verify_user_attribute_attestation
   - Signature::verify_userid_attestation
   - SignatureBuilder::pre_sign
   - SignatureBuilder::set_attested_certifications
   - SignatureType::AttestationKey
   - SubpacketAreas::MAX_SIZE
   - SubpacketAreas::attested_certifications
   - SubpacketTag::AttestedCertifications
   - SubpacketValue::AttestedCertifications
   - UserAttributeAmalgamation::attest_certifications
   - UserIDAmalgamation::attest_certifications
   - ValidUserAttributeAmalgamation::attest_certifications
   - ValidUserAttributeAmalgamation::attestation_key_signatures
   - ValidUserAttributeAmalgamation::attested_certifications
   - ValidUserIDAmalgamation::attest_certifications
   - ValidUserIDAmalgamation::attestation_key_signatures
   - ValidUserIDAmalgamation::attested_certifications
** Notable fixes
   - Improve Cert::insert_packets runtime from O(n^2) to O(n log n).
   - CertParser returned errors out of order (#699).
* Changes in 1.1.0
** New functionality
   - The new regex module provides regular expression support for
     scoping trust signatures.
   - Sequoia now supports the Cleartext Signature Framework.
   - ComponentAmalgamation::signatures
   - ComponentBundle::signatures
   - Fingerprint::to_spaced_hex
   - HashAlgorithm::text_name
   - KeyHandle now implements FromStr
   - KeyHandle::is_invalid
   - KeyHandle::to_hex
   - KeyHandle::to_spaced_hex
   - KeyID::to_spaced_hex
   - Signature4::hash_for_confirmation
   - Signature::hash_for_confirmation
   - TSK::armored
   - ValidComponentAmalgamation::signatures
** Notable fixes
   - Fixed two crashes related to detached signature verification.
   - Fixed a parsing bug where the parser did not consume all data in
     an compressed data packet.

* Changes in 1.0.0

This is the initial stable release.
